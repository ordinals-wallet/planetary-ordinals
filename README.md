# Planetary Ordinals

Block Number: 774942
Block Height: 0000000000000000000132805883b681db7d3ec3ae4ebfeda0085030687b8485

This package randomizes the ordinals drop based on blockhash from a pre-determined future block height. Accounts are listed in order of sale, while the txids of the collection are hashed against the blockhash and sorted by proof of work. The highest difficulty hash is assigned to the first account and lowest difficulty is assigned to the last purchase.

## Running

```
npm install
npm start
```
