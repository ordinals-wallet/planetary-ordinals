const crypto = require('crypto')
const fs = require('fs')
const {
	ACCOUNTS,
	BLOCK_HASH,
	COLLECTION_TXIDS,
	COLLECTION_SIZE,
	COLLECTION_PATH
} = require('./constants')

const sha256 = (input) => {
	return crypto.createHash('sha256').update(input).digest('hex')
}

const readJson = (path) => {
	return JSON.parse(fs.readFileSync(path).toString('utf8'))
}

;(() => {
	console.log(`🚀 Randomizing ${COLLECTION_SIZE} planetary ordinals`)

	const itemRarity = readJson('static/item-rarity.json')
	const traitRarity = readJson('static/trait-rarity.json')
	const metadata = readJson('static/_metadata.json')

	itemRarity.reverse()

	if (ACCOUNTS.length !== COLLECTION_SIZE) {
		console.log(`accounts: ${ACCOUNTS.length} not equal to ${COLLECTION_SIZE}`)
		return process.exit()
	}

	if (COLLECTION_TXIDS.length !== COLLECTION_SIZE) {
		console.log(`txids: ${COLLECTION_TXIDS.length} not equal to ${COLLECTION_SIZE}`)
		return process.exit()
	}

	const values = COLLECTION_TXIDS.map((e, i) => {
		const pow_hash = sha256(`${e}${BLOCK_HASH}`)
		return {
			url: `https://ordinalsbitcoin.com/inscription/${e}i0`,
			txid: e,
			pow_hash,
			pow: parseInt(pow_hash.slice(0, 16), 16),
			og_index: i
		}
	})
		.sort((a, b) => a.pow - b.pow)
		.map((e, i) => {
			const rarity = itemRarity[e.og_index]
			const item = metadata.find((e) => e.name === rarity.name)

			return {
				url: e.url,
				txid: e.txid,
				pow_hash: e.pow_hash,
				owner: ACCOUNTS[i],
				item: {
					name: item.name,
					status: rarity.status,
					rank: rarity.rank,
					attributes: item.attributes.map((a) => ({
						...a,
						status: traitRarity.find((t) => a.value === t.name).status,
						percent: traitRarity.find((t) => a.value === t.name).percent
					}))
				}
			}
		})

	fs.writeFileSync(COLLECTION_PATH, JSON.stringify(values, null, 2))
	console.log(`✅ wrote results to ${COLLECTION_PATH}`)
})()
